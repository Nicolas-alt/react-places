<img alt="final screenshot" src="./docs/screen1.png" />


# ⚛️ Places 🌎🗺️

Project for get the nearest restaurants of your current position.


## 📐 Project setup
```
npm install
```

### 🏍️ Compiles and hot-reloads for development
```
npm start
```

### 🧱 Compiles and minifies for production
```
npm run build
```
