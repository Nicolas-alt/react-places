export interface coordsInterface {
  sw: {
    lng: number
    lat: number
  }
  ne: {
    lng: number
    lat: number
  }
  zoom: number
}
