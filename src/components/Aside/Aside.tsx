import { FC, useEffect, useState, memo } from 'react'
import { v4 as uuidv4 } from 'uuid'
import PlaceCard from '../PlaceCard/PlaceCard'
import getPlaces from '../../helpers/getPlaces'
import { coordsInterface } from '../../ts/coordsInterface'
import Loader from '../Loader/Loader'

interface props {
  coordinates: coordsInterface
  showMap: boolean
  setShowMap: (showMap: boolean) => void
}

const Aside: FC<props> = ({ showMap, coordinates }) => {
  const initialState = {
    data: [],
    loading: true
  }

  const [places, setPlaces] = useState(initialState)

  useEffect(() => {
    getPlaces(coordinates).then((response) => {
      setPlaces({
        loading: false,
        data: response.filter((response: any) => response.name !== undefined)
      })
    })
  }, [coordinates])

  return (
    <aside
      className={
        showMap
          ? 'absolute w-full lg:static lg:col-span-4 z-30 top-12 bg-white overflow-hidden'
          : 'relative hidden w-full h-screen overflow-hidden col-span-4 shadow-lg lg:block'
      }
    >
      <div className="h-full rounded-2xl dark:bg-gray-700">
        <div className="flex items-center justify-center pt-2 md:pt-16">
          <h1 className="w-full py-3 font-bold text-center text-indigo-400 capitalize shadow-lg">
            Results
          </h1>
        </div>
        <div className="w-full h-screen px-3 overflow-y-auto">
          {places.loading ? (
            <div className="flex items-center justify-center w-full h-full">
              <Loader />
            </div>
          ) : (
            places.data.map(
              ({
                name,
                photo,
                description,
                phone,
                website,
                email,
                address
              }) => (
                <PlaceCard
                  key={uuidv4()}
                  name={name}
                  photo={photo}
                  description={description}
                  phone={phone}
                  website={website}
                  email={email}
                  address={address}
                />
              )
            )
          )}
        </div>
      </div>
    </aside>
  )
}

export default memo(Aside)
