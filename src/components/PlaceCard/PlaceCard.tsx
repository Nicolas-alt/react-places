import { FC, memo } from 'react'

interface PhotoInterface {
  images: {
    medium: {
      url?: string
    }
  }
}

interface Props {
  name: string
  photo: PhotoInterface
  description: string
  phone: string
  website: string
  email: string
  address: string
}

const PlaceCard: FC<Props> = ({
  name,
  photo,
  address,
  description,
  email,
  phone,
  website
}) => {
  const {
    images: {
      medium: { url }
    }
  } = photo

  return (
    <article className="w-full m-auto my-5 overflow-hidden rounded-lg shadow-lg h-90 md:w-full">
      <div className="block w-full h-full">
        <img alt="blog" src={url} className="object-cover w-full max-h-40" />
        <div className="w-full p-4 bg-white dark:bg-gray-800">
          <p className="flex items-center py-2 font-medium text-indigo-500 text-md">
            <i className="mr-1 text-xl bx bx-phone" />
            {phone}
          </p>
          <p className="flex items-center my-2 text-justify text-gray-400">
            <i className="mr-1 text-xl text-indigo-400 bx bx-current-location" />
            {address}
          </p>
          <p className="mb-2 text-xl font-medium text-gray-800 dark:text-white">
            {name}
          </p>
          <p className="font-light text-justify text-gray-400 text-md">
            {description}
          </p>
          <div className="flex justify-between py-2">
            {website && (
              <a
                href={website}
                target="_blank"
                rel="noopener noreferrer"
                className="text-indigo-400"
              >
                Website
              </a>
            )}
            {email && (
              <p className="flex items-center text-gray-800">
                <i className="mt-1 mr-1 text-indigo-400 bx bx-mail-send" />
                {email}
              </p>
            )}
          </div>
        </div>
      </div>
    </article>
  )
}

PlaceCard.defaultProps = {
  photo: {
    images: {
      medium: {
        url: 'https://images.unsplash.com/photo-1414235077428-338989a2e8c0?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80'
      }
    }
  }
}

export default memo(PlaceCard)
