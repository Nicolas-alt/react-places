import { useState } from 'react'
import Aside from '../Aside/Aside'
import Header from '../Header/Header'
import Map from '../Map/Map'
import { coordsInterface } from '../../ts/coordsInterface'

const initialCoords: coordsInterface = {
  sw: {
    lng: -74.2121362826438,
    lat: 4.534201812657443
  },
  ne: {
    lng: -73.927731518002,
    lat: 4.651099094307909
  },
  zoom: 2
}

const App = () => {
  const [showMap, setShowMap] = useState(false)
  const [coordinates, setCoordinates] = useState(initialCoords)

  return (
    <>
      <Header showMap={showMap} setShowMap={setShowMap} />
      <div className="grid h-screen max-h-screen grid-cols-1 md:grid-cols-12">
        <Aside
          showMap={showMap}
          setShowMap={setShowMap}
          coordinates={coordinates}
        />
        <Map
          showMap={showMap}
          coordinates={coordinates}
          setCoordinates={setCoordinates}
        />
      </div>
    </>
  )
}

export default App
