import { FC, useEffect, useRef, memo, useState } from 'react'
import mapboxgl from 'mapbox-gl'
import { coordsInterface } from '../../ts/coordsInterface'

const keyCredential = process.env.REACT_APP_TOKEN_MAP || ''
mapboxgl.accessToken = keyCredential

interface props {
  coordinates: coordsInterface
  setCoordinates: Function
  showMap: boolean
}

const Map: FC<props> = ({ showMap, coordinates, setCoordinates }) => {
  const mapRef = useRef<HTMLDivElement>(null)
  const [map, setMap] = useState<any>()
  const [location, setLocation] = useState({ lat: 4.5, lng: -73 })

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {
        setLocation({
          lat: latitude,
          lng: longitude
        })
      }
    )
  }, [])

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const map = new mapboxgl.Map({
      container: mapRef.current || '',
      style: 'mapbox://styles/mapbox/streets-v8',
      center: [location.lng, location.lat],
      zoom: 10
    })
    setMap(map)
  }, [location.lat, location.lng])

  useEffect(() => {
    map?.on('mouseup', () => {
      const { _sw, _ne } = map.getBounds()
      setCoordinates({
        sw: {
          lng: _sw.lng,
          lat: _sw.lat
        },
        ne: {
          lng: _ne.lng,
          lat: _ne.lat
        },
        zoom: map.getZoom()
      })
    })
  }, [map, setCoordinates])

  return (
    <div className="relative h-full col-span-12 lg:col-span-8">
      <div ref={mapRef} className="w-full h-full" />
    </div>
  )
}

export default memo(Map)
