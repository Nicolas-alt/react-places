import axios from 'axios'
import { coordsInterface } from '../ts/coordsInterface'

const url = 'https://travel-advisor.p.rapidapi.com/restaurants/list-in-boundary'

const getPlaces = async (data: coordsInterface) => {
  const options = {
    params: {
      bl_latitude: data.sw.lat,
      tr_latitude: data.ne.lat,
      bl_longitude: data.sw.lng,
      tr_longitude: data.ne.lng,
      limit: '15'
    },
    headers: {
      'x-rapidapi-key': process.env.REACT_APP_API_RESTAURANT,
      'x-rapidapi-host': 'travel-advisor.p.rapidapi.com'
    }
  }
  try {
    const {
      data: { data }
    } = await axios.get(url, options)

    return data
  } catch (error) {
    console.log(error)
  }
}

export default getPlaces
